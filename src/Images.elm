module Images exposing (ImageType(..), boundsToVec2, textureAtlas, uvCoordinate)

import Base64
import BoundingBox2d exposing (BoundingBox2d)
import Image exposing (Image)
import List.Extra as List
import Math.Vector2 as Vec2 exposing (Vec2)
import Pack exposing (TextureAtlas)
import Pixels
import Quantity exposing (Quantity, Unitless)


type ImageType
    = Key
    | KeyPressed
    | Font
    | Run Int


key : String
key =
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AQNCSUaXKufOwAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAb0lEQVRYw+3XMQ7AIAgFUDA9iQncf+UqmnCVdipR21VcPhO68CQun4mIVPWmA9Va42u9FJEtw2qt0ZtZ9Dy+ftfwP8iLKNlrd/fpXOhwAQAAAAAAAAAAAAAAAAAToPeeMnRMRp9oloXAH4hwejqeP1GTGZrGdjPVAAAAAElFTkSuQmCC"


keyPressed : String
keyPressed =
    "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH5AQNCRsmMgH4wQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAbklEQVRYw+3XMQ7AIAgF0K/pSUjk/itX0cSrtJOttl2F5TOpy38SFgBWcCUAUNUzIrzWmo73YyllS5iI3Gczezow/35X+B9kILJ323vvyz1HDyEBBBBAAAEEEEAAAQQQsABaay6h82b0Wc28EKMu9FYVlWY0+0oAAAAASUVORK5CYII="


mogeeFont : String
mogeeFont =
    "iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AQNDhwif2K8mgAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAGxUlEQVR42u1dSXLjMAyMWPr/lz0nV7E4WBoLJcvuviRObG4CsTRB+O+PICy8Xq+X9Hr9e7ZtrT3vtTfOSF9oG3esd1d7c7vz7+edEz2O47ijr+M4Dqn/q8bzSWs8pN0xS8h7sbQGVwmTJM36qe10q9/1c+vv1hi6tIi2btrcMmtgvd8aSwRD2wk71eG7P086pZ3qvV7Hj/blqU6t37n97Dqu7/M+N/cZ+ZwkJAORfE1l7lZZ79c7hNFqd+5Xm7P0+coada+ttjHWvw3tH3dAU/sZIfDm4z3ctQ1JIDzzeKfzh67ZeL9Rsr0VdbkjaoiYgSu0k+ebIJoFUePI/No1ZaWxDoer83PSZ9C/eTb0SSG8GgXscvKeHB49cV4ZrXD+/SC0UNZ6wMh7njDXR7ORT1K/XwuJ8NAcP8Qx1AgNJJ71iCZJaKTXCGHjkUGWT4HS2uicPeIr5KtUnSp08SJC4QmKdz6BCkCEtYzMa7fzm4m2NKEQqWCPquxywCL2qcJV3GVC0MOniABX1lxyCk/vQbz/F41FtXMDzwHTPNf1fRGnbI69dxIuFfJph+AhfQ3ELqGL5rF32t8kMsSjMT+FvbR21xVxvPR8tPGoJqAjlEAPlVYtcdVueVo4hM4f3Qhae8OTlPX/nfZ0paFniZZMReTYF2H1tIWLHG1Lc0CJGUtrVM8VpHOL2+jGT6VBK+1FPH7PqbuT5xh/X4puie/KHtqda/H4nfZNZBlK7vzkOu4kTbh+OM6ot63F45qTszpc0hm6FrNnIwKrv/U90ni9eWbNANoX+v51nigX8V+bkTjfc1wi1KnXfpaFRPrLUL3VnVfpKztebwwDsU27VfEatmiEknXogk4cDdeiDxVpH2FSM44pcoytmoBV7SLqM0NFIguzZtpqE9UyfqOsHKqNOmy0ZtIkKhlNRkXMSFltRk70Ok3ADlWfGRtiAiLmKnoHAB2bdmyt3Vd4vx6IlO00AVkb72XoVmLwyJg8c7Uuvnfw5r0HZQ6tOwvz69PyJq2Tuq70qYiAed5ydJzVeWbsuXToJUVSlinTTECL+id+jHzjwycIgnjCpY+u0HRuZyCL0lkMAs2Izdzcsbxvr83ZM64Wq4iEzVYId4VQDS+86ThW9dpC8vUiC3JM6N5hu/P6vLOLStsScziiUoeWddHakybhMZBIbIwWS/By+KV43hoXcn6RyanMkklttkGakJUyjl7UqOzCqKpEc/wjdYqsB+ytj9ZG5r5B9NJJ2AR4hIbHXmUZPuv4dZeJQo+LJfIFKa6hmajswZa17vP6IT7cqKojzw6jO1xStd4BEqpuM74AWpyiO3N5XbdIPoPWljXGUVHX2iJoRSe8ZBDLJ5DGN+8kK0sXyTreIRyez7JjDOHxtDsUZCEfzRVwoHz4BEH8pvqoUJxI7JohnCq0qMVXZIpS7FC1mQug6BwszqFkOyoC0NVfNgk0m7J2lwB0puGJh0FI/d+ohkDZs05mz3tftfZhhB+J1BBG+62QWusZwH9nAWiBw05SaGXDkEMQ7afG1mXrBFfWAalC3pFn6XEf6KYaGjWZrYPbeZSpEUfSFe0uH2glWLTE04jwd7CT3qbS6GWoXHyE3UMGteu4dNfOlhbUOgvIZg3vitkrbZ3ZVOlVW2TP3qOZrdLDR6p7e313R1TRKuPaLkYvguzIbH40A3V3ZbAd7eyYS0uBiG/7qpVPxY51Prmsfbu0yxnlpiIuM7NjZycIIYLcj0frDCOOa7eN9ujXag3laD0Fq69bpCxLXyKLFBn3jrCrQjN33ACutGs6gV4yJSr9d3jvkSRWb/EiCaAVhw1Jk+sQ6nSlUG1Aa9zqETRIUchoPI8UtMgSR1FeveLdo7mN3Vr6rEhP9Zu8qhOqVjKJznOnZ56hcTPPYp3LQD9opTdfNfGOAgveWKxdXxGkCHMaZRIjz2J9z+kVSLAGd1XM6t09yGoCtHJHpHCztpbRMWol4jIbK11IoutS6NXxcvQbNbpyBZ6IsfthXD0h79tAqx77z7BOT5Ru757fDhKI0kI8GseOXZhRsVr2z9UO50+qe0SddbBOsypGzwGs34kNu9a6zx79QsYIyYIeZlAAmqMA9Cq2xw9EVLxVPYQP+IYwUKtUuSNauOq784iAAKwVstCdyAf5fJxe9qhVhDmzm5HLINZnKHQf4Che0QZ9gQtNwGWkw3SFiV8a9YMagCAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiAIgiB+GP8ABh50TON684QAAAAASUVORK5CYII="


run0 : String
run0 =
    "iVBORw0KGgoAAAANSUhEUgAAAB4AAAA0CAYAAACdB4jyAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AQNCysHoqoKAgAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAp0lEQVRYw+1WRw7AIAyLEf//snvtBYmRwQjnUuN4gEjQQsc3VP6fiIhUJcDhVaJGncD3A1dHrH9KUJw7oWvUMGKrPmo2Ds4ZVhbthQhXIyJOGMkxTskxWrn1LBBkVyfwW8C9BaH+xE2N01yjLwxYMMbzGjNd/QwwZnSHgamgCcyFKbhpvPe1WL0YXhMneG2kFnCJYHuMxtzBXPDMIDUPsKoxI801Bf4BxckUccJmEEsAAAAASUVORK5CYII="


run1 : String
run1 =
    "iVBORw0KGgoAAAANSUhEUgAAAB4AAAA0CAYAAACdB4jyAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AQNCysQIXmPxQAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAyElEQVRYw+2YwQ7DIAxD64j//2XvtsO0sjbFDu3gwqXixSaxULetaGHAGcwwIAR2OWGAfl1RAc2AOaq5oqqBw6XwDJg7u3SOqWZAbC2OWE1RsnUrosiJrmIe+BjOrpbC4+NguODKyOwW2ZSdW53Vc4FbAZOZLuW6Y8fzliPPjipB/3fHK7kyzcPMpIR5JJ95x3SC8XiredXusudt+1EhThbGUQHCO41TWjEcqpu7qfasdv1weyvGXQJkWvB67MlG5XTkqhVzOqtfMwAecGdnzP0AAAAASUVORK5CYII="


run2 : String
run2 =
    "iVBORw0KGgoAAAANSUhEUgAAAB4AAAA0CAYAAACdB4jyAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5AQNCysWyBoq8AAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAAoklEQVRYw+2XSQ6AMAwDa8T/v2xeAN1sBYFz7CGjLE5StHnjzTtmnEAAXPKnBg/7hQE65PtoRXaYopWAW8AKc3U1VBEjNQ74c2BWgEtmNStSzTfUWD65MBApHPPaKSfOglGpY4iajKtNw43e4RPHWWPsykR+Uzt/El3fpyCNXAkk+zjg/35hetKZllRqHHDAAWcfW8BLKXUde3h4f+eVWVbjCxu2G12qsMsrAAAAAElFTkSuQmCC"


textureAtlas : TextureAtlas ImageType
textureAtlas =
    [ ( KeyPressed, keyPressed ), ( Font, mogeeFont ), ( Key, key ), ( Run 0, run0 ), ( Run 1, run1 ), ( Run 2, run2 ) ]
        |> List.filterMap
            (\( key_, data ) ->
                case Base64.toBytes data |> Maybe.andThen Image.decode of
                    Just image ->
                        Just { image = image, data = key_ }

                    Nothing ->
                        Nothing
            )
        |> Pack.textureAtlas
            { minimumWidth = Quantity.Quantity 128
            , nearestPowerOfTwoSize = True
            , spacing = Quantity.zero
            }


uvCoordinate : ImageType -> { offsetX : Float, offsetY : Float, width : Float, height : Float, topLeft : Vec2, topRight : Vec2, bottomLeft : Vec2, bottomRight : Vec2 }
uvCoordinate imageType =
    textureAtlas.packingData.boxes
        |> List.find (.data >> (==) imageType)
        |> Maybe.map
            (\{ x, y, width, height, data } ->
                let
                    textureWidth =
                        textureAtlas.packingData.width

                    textureHeight =
                        textureAtlas.packingData.height

                    toUv value size =
                        Quantity.ratio
                            value
                            (Quantity.toFloatQuantity size)
                            |> Quantity.float

                    { topLeft, topRight, bottomLeft, bottomRight } =
                        BoundingBox2d.fromExtrema
                            { minX = toUv (Quantity.plus (Quantity.toFloatQuantity x) (Pixels.pixels 0.5)) textureWidth
                            , minY = toUv (Quantity.plus (Quantity.toFloatQuantity y) (Pixels.pixels 0.5)) textureHeight
                            , maxX = toUv (Quantity.sum [ Quantity.toFloatQuantity x, Quantity.toFloatQuantity width, Pixels.pixels -0.5 ]) textureWidth
                            , maxY = toUv (Quantity.sum [ Quantity.toFloatQuantity y, Quantity.toFloatQuantity height, Pixels.pixels -0.5 ]) textureHeight
                            }
                            |> boundsToVec2
                in
                { offsetX = 0
                , offsetY = 0
                , topLeft = topLeft
                , topRight = topRight
                , bottomLeft = bottomLeft
                , bottomRight = bottomRight
                , width = rawQuantity width |> toFloat
                , height = rawQuantity height |> toFloat
                }
            )
        |> Maybe.withDefault
            { offsetX = 0
            , offsetY = 0
            , topLeft = Vec2.vec2 0 0
            , topRight = Vec2.vec2 0 0
            , bottomLeft = Vec2.vec2 0 0
            , bottomRight = Vec2.vec2 0 0
            , width = 0
            , height = 0
            }


rawQuantity : Quantity number units -> number
rawQuantity (Quantity.Quantity value) =
    value


boundsToVec2 :
    BoundingBox2d units coordinates
    -> { topLeft : Vec2, topRight : Vec2, bottomLeft : Vec2, bottomRight : Vec2 }
boundsToVec2 bounds =
    let
        { minX, minY, maxX, maxY } =
            BoundingBox2d.extrema bounds
    in
    { topLeft = Vec2.vec2 (rawQuantity minX) (rawQuantity maxY)
    , topRight = Vec2.vec2 (rawQuantity maxX) (rawQuantity maxY)
    , bottomLeft = Vec2.vec2 (rawQuantity minX) (rawQuantity minY)
    , bottomRight = Vec2.vec2 (rawQuantity maxX) (rawQuantity minY)
    }
