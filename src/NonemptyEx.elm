module NonemptyEx exposing (updateLast)

import List.Extra
import List.Nonempty


updateLast : (a -> a) -> List.Nonempty.Nonempty a -> List.Nonempty.Nonempty a
updateLast mapFunc nonempty =
    let
        index =
            List.length tail - 1

        tail =
            List.Nonempty.tail nonempty
    in
    if List.isEmpty tail then
        List.Nonempty.replaceHead (mapFunc (List.Nonempty.head nonempty)) nonempty

    else
        List.Nonempty.Nonempty (List.Nonempty.head nonempty) (List.Extra.updateAt index mapFunc tail)
