module Coordinates exposing (ScreenCoordinates, UvCoordinate, WorldCoordinates)


type ScreenCoordinates
    = ScreenCoordinates Never


type WorldCoordinates
    = WorldCoordinates Never


type UvCoordinate
    = UvCoordinate Never
