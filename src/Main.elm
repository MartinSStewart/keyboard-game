module Main exposing (..)

import Browser
import Browser.Events
import Camera3d
import Coordinates exposing (WorldCoordinates)
import Direction3d
import Element
import Geometry.Interop.LinearAlgebra.Point3d as Point3d
import Html exposing (Html)
import Html.Attributes
import Image
import Images exposing (ImageType)
import Json.Decode as JD
import Keyboard exposing (Key, RawKey)
import KeyboardHelper
import List.Nonempty exposing (Nonempty(..))
import Math.Matrix4 exposing (Mat4)
import Math.Vector2 as Vec2 exposing (Vec2)
import Math.Vector3 as Vec3 exposing (Vec3)
import MogeeFont
import Pixels exposing (Pixels)
import Point2d exposing (Point2d)
import Point3d exposing (Point3d)
import Quantity exposing (Quantity, Unitless)
import Task
import Time
import Vector2d
import Vector3d
import Viewpoint3d
import WebGL exposing (Mesh, Shader)
import WebGL.Matrices
import WebGL.Settings
import WebGL.Settings.Blend as Blend
import WebGL.Settings.DepthTest as Depth
import WebGL.Texture as Texture exposing (Texture)



---- MODEL ----


type alias Model =
    { windowSize : ( Quantity Int Pixels, Quantity Int Pixels )
    , gameState : GameState
    , keys : List Key
    , previousKeys : List Key
    , time : Time.Posix
    }


type alias KeyboardLayout =
    Nonempty KeyboardRow


type alias KeyboardRow =
    { offset : Quantity Float Pixels, row : List Keyboard.Key }


type GameState
    = Loading
    | FailedToLoad Texture.Error
    | GetLayout GetLayout_
    | Playing { spriteSheet : Texture, keyboardLayout : KeyboardLayout }
    | Dead { spriteSheet : Texture, keyboardLayout : KeyboardLayout }


type alias GetLayout_ =
    { spriteSheet : Texture, keyboardLayout : KeyboardLayout }


type alias Flags =
    { windowSize : ( Quantity Int Pixels, Quantity Int Pixels )
    , time : Time.Posix
    }


decodeFlags : JD.Decoder Flags
decodeFlags =
    JD.map3
        (\width height time ->
            { windowSize = ( Pixels.pixels width, Pixels.pixels height )
            , time = Time.millisToPosix time
            }
        )
        (JD.field "windowWidth" JD.int)
        (JD.field "windowHeight" JD.int)
        (JD.field "time" JD.int)


init : JD.Value -> ( Model, Cmd Msg )
init json =
    let
        initHelper windowSize time =
            ( { windowSize = windowSize
              , gameState = Loading
              , keys = []
              , previousKeys = []
              , time = time
              }
            , Images.textureAtlas.atlas
                |> Image.toPngUrl
                |> Texture.loadWith
                    { magnify = Texture.nearest
                    , minify = Texture.nearest
                    , horizontalWrap = Texture.clampToEdge
                    , verticalWrap = Texture.clampToEdge
                    , flipY = False
                    }
                |> Task.attempt LoadSpriteSheet
            )
    in
    case JD.decodeValue decodeFlags json of
        Ok flags ->
            initHelper flags.windowSize flags.time

        Err _ ->
            initHelper ( Pixels.pixels 1920, Pixels.pixels 1080 ) (Time.millisToPosix 0)


camera : Quantity Int Pixels -> Camera3d.Camera3d Pixels WorldCoordinates
camera windowHeight =
    Camera3d.orthographic
        { viewpoint =
            Viewpoint3d.lookAt
                { focalPoint = Point3d.origin
                , eyePoint = Point3d.pixels 0 0 1
                , upDirection = Direction3d.y
                }
        , viewportHeight = Quantity.toFloatQuantity windowHeight
        }



---- UPDATE ----


type Msg
    = NoOp
    | LoadSpriteSheet (Result Texture.Error Texture)
    | WindowResize ( Quantity Int Pixels, Quantity Int Pixels )
    | KeyMsg Keyboard.Msg
    | AnimationFrame Time.Posix


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        LoadSpriteSheet result ->
            case result of
                Ok texture ->
                    ( { model
                        | gameState =
                            GetLayout
                                { spriteSheet = texture
                                , keyboardLayout = Nonempty { offset = Quantity.zero, row = [] } []
                                }
                      }
                    , Cmd.none
                    )

                Err error ->
                    ( { model | gameState = FailedToLoad error }
                    , Cmd.none
                    )

        WindowResize windowSize ->
            ( { model | windowSize = windowSize }, Cmd.none )

        KeyMsg keyMsg ->
            let
                newPressedKeys =
                    Keyboard.update keyMsg model.keys

                newDownKeys : List Key
                newDownKeys =
                    newPressedKeys
                        |> List.filter (\key -> List.all ((/=) key) model.keys)
            in
            ( { model
                | keys = newPressedKeys
                , gameState =
                    case model.gameState of
                        GetLayout state ->
                            updateKeyboardLayout newDownKeys state |> GetLayout

                        _ ->
                            model.gameState
              }
            , Cmd.none
            )

        AnimationFrame time ->
            ( { model | time = time, previousKeys = model.keys }
            , Cmd.none
            )


updateKeyboardLayout : List Key -> GetLayout_ -> GetLayout_
updateKeyboardLayout newDownKeys state =
    { state
        | keyboardLayout =
            if newDownKeys |> List.any ((==) Keyboard.Backspace) then
                if isCurrentRowEmpty state.keyboardLayout then
                    state.keyboardLayout
                        |> List.Nonempty.tail
                        |> List.Nonempty.fromList
                        |> Maybe.withDefault state.keyboardLayout

                else
                    let
                        { offset, row } =
                            currentRow state.keyboardLayout
                    in
                    List.Nonempty.replaceHead
                        { offset = offset
                        , row =
                            case row of
                                _ :: rest ->
                                    rest

                                [] ->
                                    []
                        }
                        state.keyboardLayout

            else if newDownKeys |> List.any ((==) Keyboard.Enter) then
                if isCurrentRowEmpty state.keyboardLayout then
                    state.keyboardLayout

                else
                    List.Nonempty.cons
                        { offset = Quantity.zero, row = [] }
                        state.keyboardLayout

            else
                let
                    { offset, row } =
                        currentRow state.keyboardLayout

                    newCharacters =
                        List.filter
                            (\key ->
                                case key of
                                    Keyboard.Character _ ->
                                        List.Nonempty.all (\keyboardRow -> List.all ((/=) key) keyboardRow.row) state.keyboardLayout

                                    _ ->
                                        False
                            )
                            newDownKeys
                in
                List.Nonempty.replaceHead
                    { offset = offset, row = newCharacters ++ row }
                    state.keyboardLayout
    }


isCurrentRowEmpty : KeyboardLayout -> Bool
isCurrentRowEmpty keyboardLayout =
    currentRow keyboardLayout |> .row |> List.isEmpty


currentRow : KeyboardLayout -> KeyboardRow
currentRow keyboardLayout =
    List.Nonempty.head keyboardLayout



---- VIEW ----


view : Model -> Html Msg
view model =
    case model.gameState of
        Loading ->
            Html.text "Loading"

        FailedToLoad _ ->
            Html.text "Error loading texture"

        GetLayout { spriteSheet, keyboardLayout } ->
            viewHelper model spriteSheet keyboardLayout

        Playing { spriteSheet, keyboardLayout } ->
            viewHelper model spriteSheet keyboardLayout

        Dead { spriteSheet, keyboardLayout } ->
            viewHelper model spriteSheet keyboardLayout


viewHelper model spriteSheet keyboardLayout =
    let
        ( width, height ) =
            model.windowSize

        camera_ : Mat4
        camera_ =
            WebGL.Matrices.viewProjectionMatrix
                (camera height)
                { nearClipDepth = Pixels.pixels 0.1
                , farClipDepth = Pixels.pixels 10
                , aspectRatio =
                    Quantity.ratio
                        (Quantity.toFloatQuantity width)
                        (Quantity.toFloatQuantity height)
                }

        keyboardMesh_ =
            keyboardMesh model keyboardLayout
    in
    WebGL.toHtmlWith
        [ WebGL.clearColor 0.4 0.5 0.6 1 ]
        [ Html.Attributes.width (Pixels.inPixels width)
        , Html.Attributes.height (Pixels.inPixels height)
        ]
        [ WebGL.entityWith
            [ WebGL.Settings.cullFace WebGL.Settings.back
            , Depth.always { write = True, near = 0, far = 1 }
            , Blend.add Blend.one Blend.oneMinusSrcAlpha
            ]
            vertexShader
            fragmentShader
            keyboardMesh_
            (uniforms camera_ spriteSheet)
        ]
        |> Element.html
        |> Element.el []
        |> Element.layout []


type alias ShaderUniforms =
    { view : Mat4, spriteSheet : Texture }


uniforms : Mat4 -> Texture -> ShaderUniforms
uniforms viewMatrix texture =
    { view = viewMatrix
    , spriteSheet = texture
    }


type alias Vertex =
    { position : Vec3, coord : Vec2 }


spriteQuad : Point2d Pixels coordinate -> ImageType -> List ( Vertex, Vertex, Vertex )
spriteQuad position imageType =
    let
        { x, y } =
            Point2d.toPixels position

        { offsetX, offsetY, width, height, topLeft, topRight, bottomLeft, bottomRight } =
            Images.uvCoordinate imageType

        x0 =
            x + offsetX

        y0 =
            y + offsetY
    in
    [ ( { position = Vec3.vec3 x0 y0 0, coord = topLeft }
      , { position = Vec3.vec3 (x0 + width) y0 0, coord = topRight }
      , { position = Vec3.vec3 (x0 + width) (y0 + height) 0, coord = bottomRight }
      )
    , ( { position = Vec3.vec3 x0 (y0 + height) 0, coord = bottomLeft }
      , { position = Vec3.vec3 x0 y0 0, coord = topLeft }
      , { position = Vec3.vec3 (x0 + width) (y0 + height) 0, coord = bottomRight }
      )
    ]


letterQuad : Point2d Pixels coordinate -> MogeeFont.Letter -> List ( Vertex, Vertex, Vertex )
letterQuad position letter =
    let
        { x, y, width, height, textureX, textureY } =
            letter

        uvCoordinates =
            Images.uvCoordinate Images.Font

        pos =
            Point2d.toPixels position

        x0 =
            pos.x + x

        y0 =
            pos.y + y

        txMin =
            Vec2.getX uvCoordinates.bottomLeft + (textureX / toFloat (Pixels.inPixels Images.textureAtlas.packingData.width))

        tyMin =
            Vec2.getY uvCoordinates.bottomLeft + (textureY / toFloat (Pixels.inPixels Images.textureAtlas.packingData.height))

        txMax =
            Vec2.getX uvCoordinates.bottomLeft + ((textureX + width) / toFloat (Pixels.inPixels Images.textureAtlas.packingData.width))

        tyMax =
            Vec2.getY uvCoordinates.bottomLeft + ((textureY + height) / toFloat (Pixels.inPixels Images.textureAtlas.packingData.height))

        bottomLeft =
            Vec2.vec2 txMin tyMin

        bottomRight =
            Vec2.vec2 txMax tyMin

        topLeft =
            Vec2.vec2 txMin tyMax

        topRight =
            Vec2.vec2 txMax tyMax
    in
    [ ( { position = Vec3.vec3 x0 y0 0, coord = topLeft }
      , { position = Vec3.vec3 (x0 + width * 2) y0 0, coord = topRight }
      , { position = Vec3.vec3 (x0 + width * 2) (y0 + height * 2) 0, coord = bottomRight }
      )
    , ( { position = Vec3.vec3 x0 (y0 + height * 2) 0, coord = bottomLeft }
      , { position = Vec3.vec3 x0 y0 0, coord = topLeft }
      , { position = Vec3.vec3 (x0 + width * 2) (y0 + height * 2) 0, coord = bottomRight }
      )
    ]


textMesh : Point2d Pixels coordinate -> String -> List ( Vertex, Vertex, Vertex )
textMesh position text =
    let
        { offsetX, offsetY, width, height, topLeft, topRight, bottomLeft, bottomRight } =
            Images.uvCoordinate Images.Font
    in
    MogeeFont.text (letterQuad position) text


mapTriple mapFunc ( a, b, c ) =
    ( mapFunc a, mapFunc b, mapFunc c )


keyboardMesh : Model -> KeyboardLayout -> Mesh { position : Vec3, coord : Vec2 }
keyboardMesh model layout =
    let
        rowHeight =
            Vector2d.pixels 0 36
    in
    layout
        |> List.Nonempty.toList
        |> List.indexedMap
            (\verticalIndex { offset, row } ->
                let
                    offset_ =
                        Vector2d.xy offset Quantity.zero

                    columnWidth =
                        Vector2d.pixels 36 0
                in
                row
                    |> List.reverse
                    |> List.indexedMap
                        (\index key ->
                            let
                                position =
                                    Point2d.translateBy offset_ Point2d.origin
                                        |> Point2d.translateBy
                                            (Vector2d.scaleBy (toFloat index) columnWidth)
                                        |> Point2d.translateBy
                                            (Vector2d.scaleBy (toFloat verticalIndex) rowHeight)

                                keyTextPosition =
                                    if KeyboardHelper.isDown model key then
                                        position |> Point2d.translateBy (Vector2d.pixels 12 0)

                                    else
                                        position |> Point2d.translateBy (Vector2d.pixels 12 2)

                                keyText =
                                    textMesh keyTextPosition (keyToString key)
                            in
                            if KeyboardHelper.isDown model key then
                                keyText |> (++) (spriteQuad position Images.KeyPressed)

                            else
                                keyText |> (++) (spriteQuad position Images.Key)
                        )
                    |> List.concat
            )
        |> List.concat
        |> WebGL.triangles


keyToString : Key -> String
keyToString key =
    case key of
        Keyboard.Character text ->
            text

        _ ->
            ""


vertexShader : Shader { position : Vec3, coord : Vec2 } { a | view : Mat4 } { vcoord : Vec2 }
vertexShader =
    [glsl|
        attribute vec3 position;
        attribute vec2 coord;
        uniform mat4 view;
        varying vec2 vcoord;

        void main () {
          gl_Position = view * vec4(position, 1.0);
          vcoord = coord;
        }
    |]


fragmentShader : Shader {} { a | spriteSheet : Texture } { vcoord : Vec2 }
fragmentShader =
    [glsl|
        uniform sampler2D spriteSheet;

        precision mediump float;
        varying vec2 vcoord;

        void main () {
            gl_FragColor = texture2D(spriteSheet, vcoord);
        }
    |]



---- SUBSCRIPTION ----


subscriptions _ =
    Sub.batch
        [ Browser.Events.onResize
            (\width height -> WindowResize ( Pixels.pixels width, Pixels.pixels height ))
        , Sub.map KeyMsg Keyboard.subscriptions

        --, Browser.Events.onAnimationFrame AnimationFrame
        ]



---- PROGRAM ----


main : Program JD.Value Model Msg
main =
    Browser.element
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        }
